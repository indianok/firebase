import * as functions from "firebase-functions";
import * as admin from "firebase-admin"
admin.initializeApp()

const db = admin.firestore()

const region = "europe-west1"

const users = "users"

export const onUserCreate = functions
    .region(region)
    .auth.user()
    .onCreate(async (user) => {
        await db.collection(users).doc(user.uid).create({ admin: false });
    })

export const onUserDelete = functions
    .region(region)
    .auth.user()
    .onDelete(async (user) => {
        await db.collection(users).doc(user.uid).delete();
    })

export const onUserWrite = functions
    .region(region)
    .firestore.document(`${users}/{userId}`)
    .onWrite((change, context) => {
        const uid = context.params.userId
        const data = change.after.data()
        return admin.auth().setCustomUserClaims(uid, data ?? null)
    })

